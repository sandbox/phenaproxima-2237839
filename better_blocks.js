(function ($, _, Backbone, Drupal, settings) {

  var regions = {};

  var Block = Backbone.Model.extend({
    initialize: function() {
      this.on('change:region', function() {
        regions[this.previous('region')].remove(this);
        regions[this.get('region')].add(this);
      });
    }
  });

  var DisabledBlock = Backbone.View.extend({
    render: function() {
      this.el.innerHTML = this.model.get('title');
    }
  });

  var EnabledBlock = Backbone.View.extend({
    render: function() {
      var block = this.model;

      function getActionUrl(action) {
        return settings.basePath + '?q=admin/structure/block/manage/' + block.get('module') + '/' + block.get('delta') + '/' + action;
      }

      function getDisable() {
        return function(event) {
          event.preventDefault();
          block.set('region', -1);
        }
      }

      this.el.innerHTML = block.get('title');
      $('<a><i class="fa fa-wrench"></i></a>').attr('href', getActionUrl('configure')).appendTo(this.el);
      $('<a href="#"><i class="fa fa-power-off"></i></a>').on('click', getDisable()).appendTo(this.el);

      if (block.get('module') == 'block') {
        $('<a><i class="fa fa-ban"></i></a>').attr('href', getActionUrl('delete')).appendTo(this.el);
      }
    }
  });

  Drupal.behaviors.betterBlocks = {

    attach: function(context, settings) {
      $('[data-region]', context).each(function() {
        var id = $(this).data('region');
        regions[id] = new Backbone.Collection(_.where(settings.blocks, { region: id }), { comparator: 'weight', model: Block });
      });
      // Re-sort the disabled blocks by title.
      regions['-1'].comparator = 'title';
      regions['-1'].sort();

      function createSortable(regionID) {
        function sortHandler() {
          this.collection.each(function(block, index) {
            block.set('weight', index);
          });
        }

        regions[regionID].on('reorder', sortHandler);

        return new Backbone.CollectionView({
          collection:
            regions[regionID],
          detachedRendering:
            true,
          el:
            this,
          emptyListCaption:
            Drupal.t('There are no blocks in this region.'),
          modelView:
            EnabledBlock,
          sortable:
            true,
          sortableOptions: {
            axis: false
          }
        });
      }

      function onDrop() {
        return function(event, ui) {
          var origin = ui.draggable.parent('[data-region]').data('region');
          regions[origin].get(ui.draggable.data('model-cid')).set('region', $(this).next('[data-region]').data('region'));
        }
      }

      $('ul[data-region]:not([data-region = "-1"])', context).each(function() {
        var view = createSortable.call(this, $(this).data('region'));
        view.render();
        $(this).data('collection_view', view).prev().droppable({
          drop:
            onDrop(),
          hoverClass:
            'ui-state-highlight',
          tolerance:
            'touch'
        });
      });

      $('ul[data-region = "-1"]', context).each(function() {
        var collection_view = new Backbone.CollectionView({
          collection:
            regions['-1'],
          detachedRendering:
            true,
          el:
            this,
          modelView:
            DisabledBlock,
          selectMultiple:
            true
        });
        collection_view.render();
        $(this).data('collection_view', collection_view);
      });

      $('#better-blocks-arrange-buttons', context).on('submit', this._onSubmit).show();
    },

    detach: function(context) {
      function destroySortable() {
        delete $(this).data('collection_view');
        $(this).children().remove();
      }
      $('ul[data-region]', context).each(destroySortable).not('[data-region = "-1"]').prev().droppable('destroy');
      $('#better-blocks-arrange-buttons', context).off('submit', this._onSubmit).hide();
    },

    _onSubmit: function(event) {
      function encodeRegion(all, region) {
        return all.concat(region.toJSON());
      }
      this.elements.blocks.value = JSON.stringify(_.reduce(regions, encodeRegion, []), ['bid', 'region', 'weight']);
    }

  };

})(jQuery, _, Backbone, Drupal, Drupal.settings);
