Better Blocks is an IPE (in-place editing) UI for the core block system.

It hooks into the fairly useless region demo page and displays a sortable list
of the blocks in each region. You can re-arrange blocks by dragging and dropping.

Better Blocks also gives you the ability to preview your changes to the blocks'
configuration before you save them to the database. In preview mode, you can
browse your site like normal with a widget at the bottom of the screen that lets
you return to the block list, discard your changes, or commit them to the
database.

NOTE! Better Blocks is still in early alpha. You are quite likely to run into bugs
when using it. With apologies to anyone from Texas: unless you fancy yourself a cowboy, 
don’t deploy it on your production site!

DEPENDENCIES

- Core block module (obviously)
- Libraries API
- jQuery Update, preferably with the following patch applied, which adds support
  for jQuery Migrate:
https://www.drupal.org/files/issues/2156881-jquery_update-jquery_migrate_support-14.patch
- External JavaScript libraries:

	- Underscore: http://www.underscore.js.org
	- Backbone: http://www.backbonejs.org
	- Backbone.CollectionView: http://rotundasoftware.github.io/backbone.collectionView

INSTALLATION

After installing the dependencies, install the JavaScript libraries in the following locations:

- Underscore at sites/all/libraries/underscore/underscore-min.js
- Backbone at sites/all/libraries/backbone/backbone-min.js
- Backbone.CollectionView at sites/all/libraries/backbone.collectionView/backbone.collectionView.min.js

That's it; you can now go to any region demo page, and it will be taken over by Better Blocks
and turned into an uber-slick, and hopefully self-explanatory, IPE interface.

AUTHOR/MAINTAINER

phenaproxima <djphenaproxima@gmail.com>
