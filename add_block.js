(function($, Drupal) {

  Drupal.behaviors.betterBlocksAddBlock = {

    _isSortableRegion: function() {
      return $(this).next().is('[data-region]');
    },

    _createButton: function() {
      return $(document.createElement('button'))
        .addClass('add')
        .text(Drupal.t('add block...'))
        .on('click', function(event) {
          $('#better-blocks-add-block').dialog('open').get(0).elements.region.value = $(this).parent().next().data('region');
        });
    },

    _onSubmit: function(event) {
      function setRegion(block) {
        block.set('region', event.target.elements.region.value);
      }

      event.preventDefault();
      _.each($('ul[data-region = "-1"]', this).data('collection_view').getSelectedModels(), setRegion);
      $(this).dialog('close');
    },

    attach: function(context, settings) {
      $('.block-region', context).filter(this._isSortableRegion).append(this._createButton);

      $('#better-blocks-add-block', context)
        .dialog({
          autoOpen: false,
          modal: true,
          title: Drupal.t('Add block'),
          width: '50%'
        })
        .on('submit', this._onSubmit);
    },

    detach: function(context) {
      $('.block-region', context).children('button.add').remove();
      $('#better-blocks-add-block', context).dialog('destroy').off('submit', this._onSubmit);
    }

  };

})(jQuery, Drupal);
